package com.slotgame.slotapi.response;

import lombok.Data;

@Data
public class GetUserResponse {
    private int credit;
    private int loan;
    private int withdrawn;
}
