package com.slotgame.slotapi.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PlayResponse {
    private List<Integer> slots;
    private Integer credit;
}
