package com.slotgame.slotapi.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class User {
    @Id
    private String username;
    private int credit;
    private int loan;
    private int withdrawn;
    private LocalDateTime started;
}
