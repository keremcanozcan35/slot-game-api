package com.slotgame.slotapi.service;

import com.slotgame.slotapi.controller.request.CreateUserRequest;
import com.slotgame.slotapi.controller.response.CreateUserResponse;
import com.slotgame.slotapi.mapper.UserMapper;
import com.slotgame.slotapi.model.User;
import com.slotgame.slotapi.repository.UserRepository;
import com.slotgame.slotapi.controller.request.RequestAmount;
import com.slotgame.slotapi.response.GetUserResponse;
import com.slotgame.slotapi.response.PlayResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public Page<GetUserResponse> getUsers(Pageable pageable) {
         Page<User> pagedUser = userRepository.findAll(pageable);
         return pagedUser.map(this.userMapper::userToGetUserResponse);
    }

    public GetUserResponse getUser(String username) {
       User user = userRepository.findById(username).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,"User Deleted!"));
        return this.userMapper.userToGetUserResponse(user);
    }

    public CreateUserResponse createUser(CreateUserRequest request) {
        User user =  this.userMapper.fromCreateUserRequest(request);
        userRepository.save(user);
        return this.userMapper.toCreateUserResponse(user);
    }

    public void deleteUser(String username) {
        userRepository.delete(userRepository.getById(username));
    }

    private List<Integer> getRandomSlots(Random random){
        return random.ints(0, 4)
                .limit(3)
                .boxed()
                .collect(Collectors.toList());
    }

    public PlayResponse play(String username) {
        boolean isWon;
        User user = userRepository.findById(username).orElseThrow(()-> new ResponseStatusException(HttpStatus.BAD_REQUEST, "User is not found!"));

        if(user.getCredit() > 0){
            Random random = new Random();
            List<Integer> numbers =  getRandomSlots(random);
            Integer firstNumber = numbers.get(0);
            isWon = numbers.stream().allMatch(i -> i == firstNumber);

            if(isWon && ((user.getCredit() > 60 && random.nextDouble() < 0.6d)
                || (user.getCredit() > 40 && user.getCredit() <= 60 && random.nextDouble() < 0.3d))){
                numbers = getRandomSlots(random);
                Integer newFirst = numbers.get(0);
                isWon = numbers.stream().allMatch(i -> i == newFirst);
            }

            Integer newCredit = user.getCredit() - 1;
            if(isWon){
                newCredit += 10 + (10 * numbers.get(0));
            }

            user.setCredit(newCredit);
            userRepository.save(user);
            return PlayResponse.builder().slots(numbers).credit(newCredit).build();
        }

        else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You can not play with 0 credits");
        }
    }

    public void requestLoan(String username, RequestAmount request) {
        User user = userRepository.findById(username).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.BAD_REQUEST,"User is not found!"));

        Integer amount = request.getAmount();

        if(amount < 0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Users can not loan negative amounts");
        }

        user.setCredit(user.getCredit() + amount);
        user.setLoan(user.getLoan() + amount);
        userRepository.save(user);

    }

    public void requestWithdraw(String username, RequestAmount request) {
        User user = userRepository.findById(username).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, " User is not found!"));

        Integer amount = request.getAmount();
        if(user.getCredit() < amount){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Insufficient balance for withdraw");
        }

        user.setCredit(user.getCredit() - amount);
        user.setWithdrawn(user.getWithdrawn() + amount);
        userRepository.save(user);
    }
}
