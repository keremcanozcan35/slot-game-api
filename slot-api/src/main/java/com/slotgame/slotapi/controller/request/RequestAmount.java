package com.slotgame.slotapi.controller.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestAmount {
    private int amount;
}
