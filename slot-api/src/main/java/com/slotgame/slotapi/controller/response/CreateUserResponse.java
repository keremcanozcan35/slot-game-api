package com.slotgame.slotapi.controller.response;

import lombok.Data;

import java.time.LocalTime;

@Data
public class CreateUserResponse {
    private int credit;
    private int loan;
    private int withdrawn;
    private LocalTime time;
}
