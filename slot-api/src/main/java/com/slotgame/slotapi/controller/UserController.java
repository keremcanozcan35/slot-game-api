package com.slotgame.slotapi.controller;

import com.slotgame.slotapi.controller.request.CreateUserRequest;
import com.slotgame.slotapi.controller.response.CreateUserResponse;
import com.slotgame.slotapi.controller.request.RequestAmount;
import com.slotgame.slotapi.response.GetUserResponse;
import com.slotgame.slotapi.response.PlayResponse;
import com.slotgame.slotapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    
    @GetMapping
    public Page<GetUserResponse> getUsers(Pageable pageable){
        return userService.getUsers(pageable);
    }

    @GetMapping("/{username}")
    public GetUserResponse getUser(@PathVariable String username) {
        return userService.getUser(username);
    }

    @PostMapping
    public CreateUserResponse createUser(@RequestBody CreateUserRequest request){
        return userService.createUser(request);
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable String username){
        userService.deleteUser(username);
        return ResponseEntity.noContent()
                             .build();
    }

    @GetMapping("/{username}/play")
    public PlayResponse play(@PathVariable String username){
        return userService.play(username);
    }

    @PutMapping("/{username}/loan")
    public void requestLoan(@PathVariable String username, @RequestBody RequestAmount amount){
        userService.requestLoan(username, amount);
    }

    @PutMapping("/{username}/withdraw")
    public void requestWithdraw(@PathVariable String username, @RequestBody RequestAmount request){
        userService.requestWithdraw(username, request);
    }


}
