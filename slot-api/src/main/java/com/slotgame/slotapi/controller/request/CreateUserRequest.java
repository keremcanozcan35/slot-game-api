package com.slotgame.slotapi.controller.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CreateUserRequest {
    private String username;
    private LocalDateTime started;
}
