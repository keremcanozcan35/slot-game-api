package com.slotgame.slotapi.mapper;
import com.slotgame.slotapi.controller.request.CreateUserRequest;
import com.slotgame.slotapi.controller.response.CreateUserResponse;
import com.slotgame.slotapi.model.User;
import com.slotgame.slotapi.response.GetUserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    //request user response

    @Mapping(target = "time", expression = "java(user.getStarted().toLocalTime())")
    CreateUserResponse toCreateUserResponse(User user);

    @Mapping(target = "withdrawn", expression = "java(0)")
    @Mapping(target = "loan", expression = "java(0)")
    @Mapping(target = "credit", expression = "java(10)")
    @Mapping(target = "started", expression = "java(java.time.LocalDateTime.now())")
    User fromCreateUserRequest(CreateUserRequest request);

    GetUserResponse userToGetUserResponse(User user);

}
